import app from "./app";
import * as fs from 'fs';

let PORT = 3000;

try {
    let serverConfig = require('../server-config')
    PORT = serverConfig.port;
} catch (e) {
   console.log('No sever-config.json was provided, using default values');
}


app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
});


