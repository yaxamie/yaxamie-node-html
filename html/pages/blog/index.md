# Blog

* [Courageous Design](/blog/courageous_design/)

Making games without being risk averse and self sabotaging!

* [Cast Iron](/blog/cast_iron)

Why cast iron is the best thing ever.

* [Mario Run](/blog/mario_run)

A fun mobile game.

* [On Criticism](/blog/on_criticism)

Why you should seek out criticism.

* [PM2](/blog/pm2)

Process Management (How I run the server in production)
