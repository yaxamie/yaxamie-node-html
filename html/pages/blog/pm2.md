# PM2

The process manager, pm2 is a pretty cool.  I was pretty clueless the best way to go about daemonizing this website.

Enter pm2.

`pm2 start dist/server.js`

Let's see if it's running:

`pm2 ls`

Wow something looks wrong....

`pm2 log server`

Hold up, what if I want to restart my computer... 

`pm2 ls` no longer shows my server, better start it again...

`pm2 startup` and `pm save` really shine here... they take the work of saving your process list and doing startup script stuff so that the computer starts up pm2 and your processes like they were when it rebooted!

Pretty cool.

Next I'll try to do certbot stuff.

